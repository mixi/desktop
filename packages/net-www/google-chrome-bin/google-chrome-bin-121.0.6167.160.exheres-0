# Copyright 2017-2018 Rasmus Thomsen <cogitri@exherbo.org>
# Distributed under the terms of the GNU General Public License v2
# Based in part upon 'chromium.exlib', which is:
#       Copyright 2009, 2010 Elias Pipping <pipping@exherbo.org>
#       Copyright 2009 Heiko Przybyl <zuxez@cs.tu-berlin.de>
#       Copyright 2016-2017 Timo Gurr <tgurr@exherbo.org>
# Based in part upon 'google-chrome-59.0.3071.86.ebuild' from Gentoo, which is:
#     Copyright 1999-2017 Gentoo Foundation

require gtk-icon-cache freedesktop-desktop

SUMMARY="A fast, secure, and free web browser built for the modern web"
HOMEPAGE="https://www.google.com/chrome/browser"
DOWNLOADS="https://dl.google.com/linux/chrome/deb/pool/main/g/google-chrome-stable/google-chrome-stable_${PV}-1_amd64.deb"

LICENCES="google-chrome"
SLOT="0"
PLATFORMS="~amd64"
MYOPTIONS="
    ( providers: qt5 qt6 )
"

RESTRICT="mirror strip test"

DEPENDENCIES="
    run:
        app-misc/ca-certificates
        dev-libs/at-spi2-atk
        dev-libs/at-spi2-core
        dev-libs/atk
        dev-libs/expat
        dev-libs/glib:2
        dev-libs/nspr
        dev-libs/nss
        net-print/cups
        sys-apps/dbus
        sys-libs/glibc
        sys-libs/libcap
        sys-libs/libgcc:*
        sys-sound/alsa-lib
        x11-apps/xdg-utils
        x11-dri/libdrm
        x11-dri/mesa
        x11-libs/cairo
        x11-libs/libX11[>=1.5.0]
        x11-libs/libxcb
        x11-libs/libXcomposite
        x11-libs/libXdamage
        x11-libs/libXext
        x11-libs/libXfixes
        x11-libs/libxkbcommon
        x11-libs/libXrandr
        x11-libs/pango
        providers:qt5? ( x11-libs/qtbase:5 )
        providers:qt6? ( x11-libs/qtbase:6 )
"

WORK="${WORKBASE}"

pkg_setup() {
    exdirectory --allow /opt
}

src_unpack() {
    default

    edo tar xf data.tar.xz
}

src_install() {
    local dest=/opt/${PN}
    edo cd opt/google/chrome/

    exeinto ${dest}
    doexe chrome
    doexe chrome_crashpad_handler chrome-management-service
    doexe chrome-sandbox
    doexe google-chrome
    doexe xdg-mime xdg-settings

    insinto ${dest}
    doins -r locales default_apps MEIPreload WidevineCdm
    doins default-app-block *.bin *.pak *.dat
    doins libEGL.so libGLESv2.so
    doins liboptimization_guide_internal.so
    doins libvk_swiftshader.so libvulkan.so.1 vk_swiftshader_icd.json
    option providers:qt5 && doins libqt5_shim.so
    option providers:qt6 && doins libqt6_shim.so

    dodir /usr/$(exhost --target)/bin
    dosym ${dest}/google-chrome /usr/$(exhost --target)/bin/google-chrome

    for size in 24 48 64 128 256 ; do
        insinto /usr/share/icons/hicolor/${size}x${size}/apps
        newins "${WORK}"/opt/google/chrome/product_logo_${size}.png google-chrome.png
    done

    insinto /usr/share/applications/
    doins "${FILES}"/google-chrome.desktop

    insinto /usr/share/metainfo
    doins "${WORK}"/usr/share/appdata/google-chrome.appdata.xml
}

pkg_postinst() {
    freedesktop-desktop_pkg_postinst
    gtk-icon-cache_pkg_postinst
}

pkg_postrm() {
    freedesktop-desktop_pkg_postrm
    gtk-icon-cache_pkg_postrm
}

