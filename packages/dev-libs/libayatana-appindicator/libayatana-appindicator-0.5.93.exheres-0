# Copyright 2023 Timo Gurr <tgurr@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require github [ user=AyatanaIndicators ] \
    vala [ vala_dep=true ] \
    cmake

SUMMARY="Ayatana Application Indicators Shared Library"

LICENCES="GPL-3 LGPL-2.1 LGPL-3"
SLOT="0"
PLATFORMS="~amd64"
MYOPTIONS=""

RESTRICT="test"

DEPENDENCIES="
    build:
        gnome-desktop/gobject-introspection:1
        virtual/pkg-config
    build+run:
        dev-libs/glib:2[>=2.58]
        dev-libs/libayatana-indicator[>=0.8.4]
        dev-libs/libdbusmenu:0.4
        x11-libs/gtk+:3[>=3.24][gobject-introspection]
"

src_configure() {
    local cmakeparams=(
        -DENABLE_BINDINGS_MONO:BOOL=FALSE
        -DENABLE_BINDINGS_VALA:BOOL=TRUE
        -DENABLE_COVERAGE:BOOL=FALSE
        -DENABLE_GTKDOC:BOOL=FALSE
        -DENABLE_TESTS:BOOL=$(expecting_tests TRUE FALSE)
        -DENABLE_WERROR:BOOL=FALSE
        -DFLAVOUR_GTK2:BOOL=FALSE
        -DFLAVOUR_GTK3:BOOL=TRUE
		-DVALA_COMPILER=${VALAC}
		-DVAPI_GEN=${VAPIGEN}
    )

    ecmake "${cmakeparams[@]}"
}

