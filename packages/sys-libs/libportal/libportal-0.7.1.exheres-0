# Copyright 2022 Marvin Schmidt <marv@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require github [ user=flatpak tag=${PV} ]
require vala [ with_opt=true ]
require xdummy [ phase=test ]
require meson

SUMMARY="GIO-style async APIs for most Flatpak portals"

LICENCES="LGPL-3"
SLOT="0"
PLATFORMS="~amd64"
MYOPTIONS="
    gobject-introspection
    gtk-doc

    vapi [[ requires = gobject-introspection ]]

    (
        providers:
            gtk3
            gtk4
            qt5
    ) [[ number-selected = at-least-one ]]
"

# tests require libportal to be built with gobject-introspection
# pytest and additional python dependencies
RESTRICT="test"

DEPENDENCIES="
    build:
        virtual/pkg-config
        gobject-introspection? ( gnome-desktop/gobject-introspection:1 )
        gtk-doc? ( dev-doc/gtk-doc )
    build+run:
        dev-libs/glib:2[>=2.58.0]
        providers:gtk3? ( x11-libs/gtk+:3[X][wayland][gobject-introspection?] )
        providers:gtk4? ( x11-libs/gtk:4.0[X][wayland] )
        providers:qt5? (
            x11-libs/qtbase:5
            x11-libs/qtx11extras:5
        )
"

MESON_SRC_CONFIGURE_PARAMS=(
    '-Dportal-tests=false'
)
MESON_SRC_CONFIGURE_OPTION_SWITCHES=(
    'gobject-introspection introspection'
    'gtk-doc docs'
    'vapi'
)
MESON_SRC_CONFIGURE_OPTION_FEATURES=(
    'providers:gtk3 backend-gtk3'
    'providers:gtk4 backend-gtk4'
    'providers:qt5 backend-qt5'
)
MESON_SRC_CONFIGURE_TESTS=(
    '-Dtests=true -Dtests=false'
)

src_test() {
    xdummy_start
    meson_src_test
    xdummy_stop
}

