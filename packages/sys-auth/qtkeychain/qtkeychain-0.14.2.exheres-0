# Copyright 2013-2018 Timo Gurr <tgurr@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require github [ user=frankosterfeld tag=${PV} ] cmake

SUMMARY="Platform-independent Qt API for storing passwords securely"
DESCRIPTION="
QtKeychain is a Qt API to store passwords and other secret data securely. If running, GNOME Keyring
is used, otherwise qtkeychain tries to use KWallet (via D-Bus), if available. In unsupported
environments QtKeychain will report an error. It will not store any data unencrypted unless
explicitly requested (setInsecureFallback( true )).
"

LICENCES="BSD-2"
SLOT="0"
PLATFORMS="~amd64 ~armv7 ~x86"
MYOPTIONS="
    libsecret [[ description = [ Libsecret storage backend ] ]]

    ( providers: qt5 qt6 ) [[ number-selected = at-least-one ]]
"

DEPENDENCIES="
    build:
        virtual/pkg-config
        providers:qt5? ( x11-libs/qttools:5 [[ note = [ for Qt5LinguistTools ] ]] )
        providers:qt6? ( x11-libs/qttools:6 )
    build+run:
        libsecret? ( dev-libs/libsecret:1 )
        providers:qt5? ( x11-libs/qtbase:5 )
        providers:qt6? ( x11-libs/qtbase:6 )
    suggestion:
        gnome-desktop/gnome-keyring:1 [[ description = [ GNOME Keyring storage backend ] ]]
        kde/kwalletmanager:4 [[ description = [ KWallet storage backend (KF5) ] ]]
"

_qtkeychain_conf() {
    local cmake_params=(
        -DBUILD_SHARED_LIBS:BOOL=TRUE
        # Isn't installed or used during build, so don't bother with it for now
        -DBUILD_TEST_APPLICATION:BOOL=FALSE
        -DBUILD_TRANSLATIONS:BOOL=TRUE
        $(cmake_option libsecret LIBSECRET_SUPPORT)
        "$@"
    )

    ecmake "${cmake_params[@]}"
}

src_configure() {
    if option providers:qt5; then
        edo mkdir "${WORKBASE}"/qt5-build
        edo pushd "${WORKBASE}"/qt5-build
        _qtkeychain_conf -DBUILD_WITH_QT6:BOOL=FALSE
        popd
    fi

    if option providers:qt6; then
        edo mkdir "${WORKBASE}"/qt6-build
        edo pushd "${WORKBASE}"/qt6-build
        _qtkeychain_conf -DBUILD_WITH_QT6:BOOL=TRUE
        popd
    fi
}

src_compile() {
    if option providers:qt5; then
        edo pushd "${WORKBASE}"/qt5-build
        ecmake_build
        edo popd
    fi

    if option providers:qt6; then
        edo pushd "${WORKBASE}/"qt6-build
        ecmake_build
        edo popd
    fi
}

src_install() {
    if option providers:qt5; then
        edo pushd "${WORKBASE}"/qt5-build
        ecmake_install
        edo popd
    fi

    if option providers:qt6; then
        edo pushd "${WORKBASE}"/qt6-build
        ecmake_install
        edo popd
    fi

    edo pushd "${CMAKE_SOURCE}"
    emagicdocs
    edo popd
}

