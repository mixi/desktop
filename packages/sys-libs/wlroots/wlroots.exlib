# Copyright 2018-2019 Danilo Spinella <danyspin97@protonmail.com>
# Distributed under the terms of the GNU General Public License v2

require gitlab [ prefix="https://gitlab.freedesktop.org" suffix=tar.bz2 new_download_scheme=true ]
require meson alternatives

SUMMARY="A modular Wayland compositor library"
DESCRIPTION="Pluggable, composable modules for building a Wayland compositor"

LICENCES="MIT"
MYOPTIONS="
    X [[ description = [ Enable X11 backend ] ]]
    vulkan
    xwayland [[
        description = [ Enable XWayland (X11 compatibility layer) ]
        requires = [ X ]
    ]]

    ( providers: eudev systemd ) [[ number-selected = exactly-one ]]
"

DEPENDENCIES="
    build:
        sys-apps/hwdata
        virtual/pkg-config
        vulkan? (
            dev-lang/glslang[>=11.0.0]
            sys-libs/vulkan-headers
        )
    build+run:
        dev-libs/libglvnd[X?]
        sys-apps/util-linux
        sys-auth/seatd[>=0.2.0]
        sys-libs/libinput[>=1.14.0]
        sys-libs/wayland[>=1.21]
        sys-libs/wayland-protocols[>=1.24]
        x11-dri/mesa[>=17.1.0][wayland][X?]
        x11-dri/libdrm[>=2.4.113]
        x11-libs/libxkbcommon[wayland][X?]
        x11-libs/pixman:1
        X? (
            x11-libs/libxcb
            x11-proto/xcb-proto
            x11-utils/xcb-util-image
            x11-utils/xcb-util-renderutil
            x11-utils/xcb-util-wm
        )
        providers:eudev? ( sys-apps/eudev )
        providers:systemd? ( sys-apps/systemd )
        vulkan? ( sys-libs/vulkan-loader[>=1.2.182][wayland][X?] )
        xwayland? ( x11-server/xwayland )
    run:
        !sys-libs/wlroots:0[<0.16.2-r1] [[
            description = [ The unslotted old version needs to be removed or updated first ]
            resolution = upgrade-blocked-before
        ]]
"

MESON_SRC_CONFIGURE_PARAMS=(
    -Dexamples=false
    -Dicon_directory=/usr/share/icons
    -Dxcb-errors=disabled
)

MESON_SRC_CONFIGURE_OPTION_FEATURES=(
    'xwayland'
)

if ever at_least 0.17; then
    DEPENDENCIES+="
        build+run:
            dev-libs/libdisplay-info
    "
    MESON_SRC_CONFIGURE_PARAMS+=( -Dsession=enabled )
fi


src_configure() {
    local allocators=( gbm )
    local backends=( drm libinput )
    local renderers=( gles2 )

    if option X; then
        backends+=( x11 )
    fi

    if option vulkan; then
        renderers+=( vulkan )
    fi

    meson_src_configure \
        -Dallocators=$(IFS=,; echo "${allocators[*]}") \
        -Dbackends=$(IFS=,; echo "${backends[*]}") \
        -Drenderers=$(IFS=,; echo "${renderers[*]}")
}

src_install() {
    local alternatives=(
        /usr/$(exhost --target)/lib/libwlroots.so{,-${SLOT}}
        /usr/$(exhost --target)/lib/pkgconfig/wlroots{,-${SLOT}}.pc
        /usr/$(exhost --target)/include/wlr{,-${SLOT}}
    )

    meson_src_install

    alternatives_for \
        _$(exhost --target)_${PN} ${SLOT} ${SLOT} \
        "${alternatives[@]}"
}

