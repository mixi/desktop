# Copyright 2010-2016 Pierre Lejeune <superheron@gmail.com>
# Distributed under the terms of the GNU General Public License v2

require gnome.org [ suffix=tar.xz ]
require vala [ vala_dep=true with_opt=true ]
require meson

SUMMARY="A GObject-based API for handling resource discovery and announcement over SSDP"
HOMEPAGE="https://wiki.gnome.org/Projects/GUPnP"

LICENCES="LGPL-2"
SLOT="1.2"
PLATFORMS="~amd64"
MYOPTIONS="
    gobject-introspection
    gui [[ description = [ Build GUI SSDP sniffer ] ]]
    gtk-doc

    gtk-doc [[ requires = gobject-introspection ]]
    vapi [[ requires = gobject-introspection ]]
"


DEPENDENCIES="
    build:
        virtual/pkg-config
        gobject-introspection? ( gnome-desktop/gobject-introspection:1[>=1.36.0] )
        gtk-doc? ( dev-doc/gi-docgen[>=2021.1] )
    build+run:
        dev-libs/glib:2[>=2.54.0]
        gnome-desktop/libsoup:2.4[>=2.26.1][gobject-introspection?]
        gui? ( x11-libs/gtk:4.0[>=4.0] )
"

REMOTE_IDS="freecode:gupnp"

UPSTREAM_RELEASE_NOTES="http://ftp.gnome.org/pub/GNOME/sources/${PN}/$(ever range 1-2)/${PNV}.news"
UPSTREAM_DOCUMENTATION="http://developer.gnome.org/${PN}/$(ever range 1-2)"

# TODO(sardemff7): Failing to bind to localhost for some reason
RESTRICT="test"

MESON_SRC_CONFIGURE_OPTION_SWITCHES=(
    'gobject-introspection introspection'
    'gtk-doc gtk_doc'
    'gui sniffer'
    vapi
)

