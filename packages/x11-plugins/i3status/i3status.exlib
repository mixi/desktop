# Copyright 2011-2012 Morgane "Sardem FF7" Glidic <sardemff7@exherbo.org>
# Distributed under the terms of the GNU General Public License v2
# Based in part upon 'i3status' from Void Linux, which is:
#      Copyright (c) 2008-2017 Juan Romero Pardines and contributors

export_exlib_phases src_prepare src_compile

SUMMARY="A small generator for status bars"
DESCRIPTION="
i3status is a small program (about 1500 SLOC) for generating a status bar for dzen2, xmobar or
similar programs. It is designed to be very efficient by issuing a very small number of system
calls, as one generally wants to update such a status line every second. This ensures that even
under high load, your status bar is updated correctly. Also, it saves a bit of energy by not
hogging your CPU as much as spawning the corresponding amount of shell commands would.
"
HOMEPAGE="http://i3wm.org/i3status"

LICENCES="BSD-3"
SLOT="0"

MYOPTIONS="
    ( libc: musl )
    pulseaudio
"

DEPENDENCIES="
    build:
        app-doc/asciidoc
    build+run:
        dev-libs/confuse
        dev-libs/yajl
        net-libs/libnl
        sys-sound/alsa-lib
        libc:musl? (
            dev-libs/libglob [[
                note = [ use the OpenBSD implementation of GLOB_TILDE ]
            ]]
        )
        pulseaudio? ( media-sound/pulseaudio )
"

DEFAULT_SRC_PREPARE_PATCHES=(
    "${FILES}"/${PN}-Allow-to-build-without-pulseaudio.patch
)

DEFAULT_SRC_INSTALL_PARAMS=(
    PREFIX=/usr/$(exhost --target)
    MANPREFIX=/usr
    SYSCONFDIR=/etc
)

i3status_src_prepare() {
    default

    if [[ $(exhost --target) == *-musl* ]]; then
        # Replace glibc GLOB_TILDE with libglob reimplementation
        edo sed -i '/include/s,glob.h,libglob/glob.h,g' \
            ${WORK}/src/process_runs.c ${WORK}/src/print_cpu_temperature.c ${WORK}/i3status.c
        edo sed -i '/GLOB_TILDE/s/glob(/g_glob(/g' ${WORK}/src/process_runs.c \
            ${WORK}/src/print_cpu_temperature.c ${WORK}/i3status.c
        edo sed -i 's/globfree(/g_globfree(/g' ${WORK}/src/process_runs.c \
            ${WORK}/src/print_cpu_temperature.c ${WORK}/i3status.c

        # Add libglob lib
        edo echo "LIBS+=/usr/$(exhost --target)/lib/libglob.so" | edo tee -a ${WORK}/Makefile
    fi
}

i3status_src_compile() {
    emake $(option pulseaudio "" "NO_PULSEAUDIO=1")
}

