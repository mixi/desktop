# Copyright 2018-2020 Danilo Spinella <danyspin97@protonmail.com>
# Distributed under the terms of the GNU General Public License v2

SCM_EXTERNAL_REFS=" package/archlinux: "
require github [ user='Alexays' ] meson toolchain-funcs

SUMMARY="Highly customizable Wayland bar for wlroots based compositors"

LICENCES="MIT"
SLOT="0"
MYOPTIONS="
    jack
    mpd [[ description = [ Build mpd module to show current MPD state ] ]]
    mpris [[ requires = [ providers: systemd  ] ]]
    network [[ description = [ Build network module to show status of network interfaces ] ]]
    pipewire
    pulseaudio
    tray [[ description = [ Build tray module to show notifications ] ]]

    ( providers: eudev systemd ) [[ number-selected = exactly-one ]]
    ( providers: libc++ libstdc++ ) [[ number-selected = exactly-one ]]
"

DEPENDENCIES="
    build:
        app-doc/scdoc[>=1.9.2]
        providers:libc++? ( dev-lang/clang:* )
    build+run:
        dev-libs/date
        dev-libs/fmt[>=8.1.1]
        dev-libs/jsoncpp:=
        dev-libs/spdlog[>=1.10.0]
        dev-cpp/libsigc++:2
        gnome-bindings/gtkmm:3[>=3.24.4]
        sys-apps/upower
        sys-libs/libinput
        sys-libs/wayland
        sys-libs/wayland-protocols
        wayland-libs/gtk-layer-shell
        x11-libs/libevdev
        x11-libs/libxkbcommon[xkbregistry]
        jack? ( media-sound/jack-audio-connection-kit )
        mpd? ( media-libs/libmpdclient )
        mpris? ( app-misc/playerctl[>=2.0.0] )
        network? ( net-libs/libnl:3.0 )
        pipewire? ( media/wireplumber[>=0.4] )
        pulseaudio? ( media-sound/pulseaudio )
        tray? (
            dev-libs/glib:2
            dev-libs/libdbusmenu:0.4
        )
        providers:eudev? ( sys-apps/eudev )
        providers:systemd? ( sys-apps/systemd )
        providers:libc++? (
            sys-libs/libc++
            sys-libs/libc++abi
        )
        providers:libstdc++? ( sys-libs/libstdc++:* )
    test:
        dev-cpp/catch[>=2.0.0]
"

if ever is_scm; then
    DEPENDENCIES+="
        test:
            dev-cpp/catch[>=3.5.1]
    "
fi

MESON_SRC_CONFIGURE_PARAMS=(
    -Dcava=disabled
    -Dgtk-layer-shell=enabled
    -Dlibevdev=enabled
    -Dlibinput=enabled
    -Dlibudev=enabled
    -Dman-pages=enabled
    -Dupower_glib=enabled
)
MESON_SRC_CONFIGURE_OPTION_SWITCHES=(
    'providers:libc++ libcxx'
)
MESON_SRC_CONFIGURE_OPTION_FEATURES=(
    'jack'
    'mpd'
    'mpris'
    'network libnl'
    'pipewire wireplumber'
    'providers:systemd systemd'
    'providers:systemd logind'
    'pulseaudio pulseaudio'
    'tray dbusmenu-gtk'
)
MESON_SRC_CONFIGURE_TESTS=(
    '-Dtests=enabled -Dtests=disabled'
)

pkg_pretend() {
    if option providers:libc++ && ! cxx-is-clang ; then
        die "libc++ is only supported by clang. Please set cc to clang."
    fi
}

